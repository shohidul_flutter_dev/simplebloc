abstract class CounterEvent {
  
  int onPrease(int counter);
}

class IncrementEvent extends CounterEvent {
  @override
  int onPrease(int counter) {
    return ++counter;
  }
}

class DecrementEvent extends CounterEvent {
  @override
  int onPrease(int counter) {
    return --counter;
  }
}
